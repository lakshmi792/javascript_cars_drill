let inventoryObject= require('./inventoryObject.cjs');

function problem2(inventoryObj=inventoryObject)
{
    if(inventoryObj.length==0)
    {
        return inventoryObj=[];
    }
    if (!Array.isArray(inventoryObj)) {
        return inventoryObj=[];
    }
    let indexOfLastCar=inventoryObj.length-1;
    let detailsOfLastCar={};

    let answer =`Last car is a ${inventoryObj[indexOfLastCar].car_make} ${inventoryObj[indexOfLastCar].car_model}`;
    detailsOfLastCar=answer;
    return detailsOfLastCar;
}
//let lastCar=problem2();
//console.log(lastCar);

module.exports= problem2;