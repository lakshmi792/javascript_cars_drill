
let inventoryObject = require('./inventoryObject.cjs');

function problem5(allCarYears=inventoryObject)
{
    if (allCarYears.length == 0) {
        
        return allCarYears = [];
     
    }
     if (!Array.isArray(allCarYears)) {
        return allCarYears = [];
    }

    let countOfCars=0;
    let carYears=[];
    for(let index=0;index<allCarYears.length;index++)
    {
        carYears.push(allCarYears[index].car_year);
    }
    
    for(let index=0;index<carYears.length;index++)
    {
        if(carYears[index]<2000)
        {
            countOfCars++;
        }
    }
    return countOfCars;
}

//let carsOlder2000 = problem5(inventoryObject);
//console.log(carsOlder2000);

module.exports =problem5;