let inventoryObject = require('./inventoryObject.cjs');

function problem3(inventoryObj=inventoryObject) {
   if (inventoryObj.length == 0) {
      return inventoryObj = [];
   }
   if (!Array.isArray(inventoryObj)) {
      return inventoryObj = [];
   }
   let sortCarModels = [];

   for (index = 0; index < inventoryObj.length; index++) {
      sortCarModels.push(inventoryObj[index].car_model);
   }

   return sortCarModels.sort();
}
l//et carModels=problem3([]);
//console.log(carModels);


module.exports = problem3;