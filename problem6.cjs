let inventoryObject= require('./inventoryObject.cjs');

function problem6(inventoryObj=inventoryObject)
{
    if(inventoryObj.length==0)
    {
        return inventoryObj=[];
    }
    if (!Array.isArray(inventoryObj)) {
        return inventoryObj=[];
    }

     let cars=[];
     for(let index=0;index<inventoryObj.length;index++)
     {
        if(inventoryObj[index].car_make === "BMW" || inventoryObj[index].car_make ==="Audi")
        {
            let BmwAudi=inventoryObj[index];
            cars.push(BmwAudi);
        }
    
        
     }
     let answer=JSON.stringify(cars);
     return answer;
}

//let bmwAndAudi=problem6(inventoryObject);
//console.log(bmwAndAudi);


module.exports = problem6;